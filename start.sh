#!/bin/bash

DLDIR="videos"
BASEURL="https://fabacademy.org"
SOURCES=("2021/presentations.html" "2020/projects.html" "2019/presentations/sessions.html")
CONDFILE="DOWNLOADED"

# Stop here if videos have been downloaded
if [ -f ${CONDFILE} ]; then
  echo "Videos have been downloaded. Exiting..."
  exit 0
fi

# Continue otherwise. Clear previous videos.
if [ -d ${DLDIR} ]; then
  rm -rf ${DLDIR}
fi

mkdir ${DLDIR}

echo "Listing videos from ${BASEURL}/${SOURCES[0]}"
VIDEOS=$(curl -s "${BASEURL}/${SOURCES[0]}" | grep -oE "(http://)([^><])*(presentation.mp4)")

for VID in ${VIDEOS}
do
#  echo ${VID}
  IFS='/' read -ra ARR <<< ${VID}
  LEN="${#ARR[@]}"
#  echo "${ARR[${LEN}-2]}"

  STUDENT_NAME="${ARR[${LEN}-2]}"
  FILE_NAME="${DLDIR}/${STUDENT_NAME}.mp4"

  echo "Downloading file ${FILE_NAME} from ${VID}"
  curl -L -o "${FILE_NAME}" "${VID}"

  # check file size
  FILE_SIZE=$(stat -c %s ${FILE_NAME})
  echo "File size: ${FILE_SIZE}"
  if [ $FILE_SIZE -eq 0 ]; then
    echo "File too small. Removing..."
    rm ${FILE_NAME}
  fi

  # check encoding
done

# Create file so that we know that all files have been downloaded successfully
touch ${CONDFILE}

exit 0
